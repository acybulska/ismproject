package hello;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface BookRepository extends JpaRepository<Book, Long>{
    Collection<Book> findByAuthor (String author);
    Collection<Book> findByTitle (String title);
    Collection<Book> findByIsbn (String isbn);
    Collection<Book> findByYear (long year);
}
