package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/counter")
public class CounterController {

    @RequestMapping(method = RequestMethod.GET)
    int readCounter() {
        CounterSingleton counterSingleton = CounterSingleton.getInstance();
        return counterSingleton.getCounter();
    }
}