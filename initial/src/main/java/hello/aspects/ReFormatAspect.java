package hello.aspects;

import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class ReFormatAspect {
	
	 @AfterReturning(pointcut = "@annotation(hello.aspects.ReFormat)", returning = "response")
	    public void formatter(JoinPoint joinPoint, Object response) {
	        System.out.println("[@AfterReturning] " + ((String) response).toUpperCase()+ "" + new Date());
	}
}  

