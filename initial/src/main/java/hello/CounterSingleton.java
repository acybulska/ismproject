package hello;

public class CounterSingleton {

    private static CounterSingleton instance = null;
    private int counter;
    private CounterSingleton() {
        counter = 0;
    }

    public static CounterSingleton getInstance() {
        if(instance == null) {
            instance = new CounterSingleton();
        }
        return instance;
    }

    public void count() {
        counter++;
    }

    public int getCounter(){
        return counter;
    }
}
