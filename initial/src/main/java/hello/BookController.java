package hello;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@RestController
@RequestMapping("/books")
public class BookController {

    private final BookRepository bookRepository;
    final static Logger logger = Logger.getLogger(BookController.class);

    @Autowired
    BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    Collection<Book> getBooks() {
        logger.info("getBooks was open");
        return this.bookRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{title}")
    Collection<Book> getBooksByTitle(@PathVariable("title") String title) {
        return this.bookRepository.findByTitle(title);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{author}")
    Collection<Book> getBooksByAuthor(@PathVariable("author") String author) {
        return this.bookRepository.findByAuthor(author);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{isbn}")
    Collection<Book> getBooksByIsbn(@PathVariable("isbn") String isbn) {
        return this.bookRepository.findByIsbn(isbn);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{year}")
    Collection<Book> getBooksByYear(@PathVariable("year") long year) {
        return this.bookRepository.findByYear(year);
    }
}