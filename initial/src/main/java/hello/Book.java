package hello;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Book {

    @GeneratedValue
    @Id
    private Long id;

    @JsonIgnore
    @ManyToOne (fetch= FetchType.LAZY)
    private Account account;

    Book() {
    }

    public Book(String author, String title, String isbn, long year, String publisher) {
        this.author=author;
        this.title=title;
        this.isbn=isbn;
        this.year=year;
        this.publisher=publisher;
    }

    private String author;
    private String title;
    private String isbn;
    private long year;
    private String publisher;

    public Account getAccount() { return account; }

    public Long getId() { return id; }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getIsbn() {
        return isbn;
    }

    public long getYear() {
        return year;
    }

    public String getPublisher() {
        return publisher;
    }
}
