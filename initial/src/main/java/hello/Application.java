package hello;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner init(AccountRepository accountRepository, BookRepository bookRepository) {
        accountRepository.save(new Account("admin","admin"));
            return (evt) -> Arrays.asList(
                    new Book("Neil Gaiman", "Mitologia nordycka", "9788374807289", 2017, "MAG"),
                    new Book("Diana Gabaldon", "Outlander", "9781784751371", 2014, "Arrow"),
                    new Book("Diana Gabaldon", "Dragonfly in Amber", "9781784751371", 1994, "Arrow"),
                    new Book("Chuck Palahniuk", "Survivor", "9780099282648", 1999, "Vintage"))
                    .forEach(
                            a -> {bookRepository.save(a);});
    }
}

