package hello;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/users")

public class AccountController {

    private final AccountRepository accountRepository;

    @Autowired
    AccountController( AccountRepository accountRepository)
    {
        this.accountRepository=accountRepository;
    }
    /*@RequestMapping(method = RequestMethod.GET, value = "/{userId}")
    Account readAccount(@PathVariable Long userId) {
        this.validateUser(userId);
        return this.accountRepository.findById(userId);
    }
*/
    @RequestMapping(method = RequestMethod.GET)
    Collection<Account> getBooks() {
        return this.accountRepository.findAll();
    }

    private void validateUser(Long userId) {
        this.accountRepository.findById(userId).orElseThrow(
                () -> new UserNotFoundException(userId));
    }
}
