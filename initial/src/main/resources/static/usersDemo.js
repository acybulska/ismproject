angular.module('demo', []).controller('Books', function($scope, $http) {
    $http.get('http://localhost:8080/books').then(function(response) {
        $scope.books = response.data;
    });
});